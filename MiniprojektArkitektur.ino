/*=======================================================
 File name: MiniprojektArkitektur.ino
 Author: Roman Grintsevich
 Compile with: Arduino IDE 1.6.13
 Date: 2017-01-06
 Description: Mini project in "Arkitektur & design"
 Purpose: school exercise.
=======================================================*/

//7 segment current sink
#include "Tlc5940.h"
#define digitOne 0
#define digitTwo 7
#define digitThree 16
#define digitFour 23
#define digitFive 32
#define digitSix 39
#define brightnessControler A0 //Potentiometer
//Buzzer
#define buzzer 4
//Button
#define menuButton 2
#define upButton 7                   
#define downButton 8

//DS3231
#include <DS3231.h> //Time module library
DS3231 Clock;
bool Century=false;
bool h12;
bool PM;
byte ADay, AHour, AMinute, ASecond, ABits;
bool ADy, A12h, Apm;
int second, oldSeconds,minute,hour,date,month,year,temperature;
int timerHour = 0, timerMinute = 0, timerSecond = 0;
int setHours = 0, setMinutes = 0, setSeconds = 0;
int alarmYear, alarmMonth, alarmDate, alarmHour, alarmMinute, alarmSecond;
//Function prototypes
void printChar(int charToPrint, int digit, int brightness);
void readDS3231(void);
void displayTime(int displayHours, int displayMinutes, int displaySeconds, int brightness);
int readBrightnessControl(void);
int dotBrightness(void);
void menu(void);
void startAlarm(void);
void timerMenu(void);
void alarmMenu(void);
void dateDots(int brightness);
void timeDots(int brightness);

uint8_t buttonState = 0;
uint8_t menuRunning = 0;
uint8_t timerMenuRunning = 0;
uint8_t alarmMenuRunning = 0;
uint8_t alarmOn = 0;
uint8_t menuState = 1; //1 set hours, 2 set minutes, 3 set seconds
uint8_t timerOn = 0;

void setup() 
{
  //7 segment
  Tlc.init();
  oldSeconds = Clock.getSecond()-1; //Used to update 7 segment value every second
  
  //DS3231
  Wire.begin();

  //Button
  pinMode(menuButton, INPUT);
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);

  //Clear alarm 1 and 2
  Clock.turnOffAlarm(1);
  Clock.turnOffAlarm(2);

  //Buzzet
  pinMode(buzzer, OUTPUT);
  
  //Get current date for alarm
  alarmYear = Clock.getYear();
  alarmMonth = Clock.getMonth(Century);
  alarmDate = Clock.getDate();

  
  //##DEBUG
  alarmHour = Clock.getHour(h12, PM);
  alarmMinute = Clock.getMinute();
  alarmSecond = Clock.getSecond();

  //##DEBUG
  //Clock.setYear(17); //Set year
  //Clock.setMonth(1); //Set month
  //Clock.setDate(6); //Set date
  //Clock.setHour(10); //Set hour
  //Clock.setMinute(48); //Set minute
  //Clock.setSecond(00); //Set second
  //Clock.setDoW(3);    //Set day of the week
}

void loop() 
{
  //Check if menu button is pushed (time setup)
  if(digitalRead(menuButton) == HIGH)
  {
    menuRunning = 1;
    delay(1000); //Debouce
    menu();
  }

  //Timer setup
  if(digitalRead(downButton) == HIGH)
  {
    //timerOn = 1; //Main
    timerMenuRunning = 1; //Timer menu loop
    delay(1000); //Debouce
    timerMenu(); //Call timer menu
  }

  //Alarm setup
  if(digitalRead(upButton) == HIGH)
  {
    //Alarm
    alarmMenuRunning = 1;
    delay(1000); //Debouce
    alarmMenu();
  }
  
  //Read RTC modul
  readDS3231();

  //Display date for 10 seconds every second minute
  if((minute % 2 == 0) && second < 11 && oldSeconds != second)
  {
    Tlc.clear();
    //Date dots
    dateDots(dotBrightness()); 
    //Display date
    displayTime(year, month, date, readBrightnessControl()); 
    oldSeconds = second; 
  }
  else
  {
    //Display time. Updates display if time has changed
    if(oldSeconds != second)
    {
      Tlc.clear();

      //Time dots
      if(second % 2 == 0)
      {
        timeDots(dotBrightness());
      }
      else
      {
        timeDots(0);
      }

      //Display time
      displayTime(hour, minute, second, readBrightnessControl());
      oldSeconds = second; 
    }
  }

  //Timer loop
  while(timerOn)
  {
    //Read RTC modul
    readDS3231();

    //Check if time has passed and exit the timer loop
    if(timerHour == 0 && timerMinute == 0 && timerSecond == 0)
    {
      //Time passed
      for(int i = 0; i < 10; i++) //Blinking display
      {
        Tlc.clear();
        timeDots(dotBrightness());
        displayTime(timerHour, timerMinute, timerSecond, readBrightnessControl());
        delay(500);
        Tlc.clear();
        timeDots(2);
        displayTime(timerHour, timerMinute, timerSecond, 10);
        delay(350);
      }
      timerOn = 0; //Exit timer loop
    }

    //Display time left on the timer
    if(oldSeconds != second && timerSecond > 0) //If one second has passed and seconds are greater than 0 decrease seconds
    {
      timerSecond--;      
      if(timerSecond < 1 && timerMinute != 0)
      {
        timerMinute--;
        timerSecond = 59; //Reset seconds
        if(timerMinute < 1 && timerHour != 0)
        {
          timerHour--;
          timerMinute = 59; //Reset minutes
        }
      }
      
      Tlc.clear();
      //Time dots
      if(timerSecond % 2 == 0)
      {
        timeDots(dotBrightness());
      }
      else
      {
        timeDots(0);
      }
      displayTime(timerHour, timerMinute, timerSecond, readBrightnessControl());
      oldSeconds = second; 
    }
  }

  //Alarm
  if(Clock.checkAlarmEnabled(1))
  {
    if(Clock.checkIfAlarm(1))
    {
      //Alarm 1 triggered
      while(alarmOn)
      {
        //Check if menu button is pushed (stop alarm)
        if(digitalRead(menuButton) == HIGH)
        {
          alarmOn = 0;
          delay(1000); //Debouce
        }

        //Turn buzzer on
        digitalWrite(buzzer, HIGH);

        //Blink alarm time on the 7 segment display
        Tlc.clear();
        timeDots(dotBrightness());
        displayTime(alarmHour, alarmMinute, alarmSecond, readBrightnessControl());
        delay(200);
        Tlc.clear();
        timeDots(0);
        displayTime(alarmHour, alarmMinute, alarmSecond, 10);
        delay(200);

        //Turn of buzzer
        digitalWrite(buzzer, LOW);
      }

      Clock.turnOffAlarm(1); //Disable alarm 1
    }
  }
}

void displayTime(int displayHours, int displayMinutes, int displaySeconds, int brightness)
{ 
  //Hours
  printChar(displayHours / 10, digitOne, 0);
  printChar(displayHours % 10, digitTwo, 0);
  printChar(displayHours / 10, digitOne, brightness);
  printChar(displayHours % 10, digitTwo, brightness);

  //Minutes
  printChar(displayMinutes / 10, digitThree, 0);
  printChar(displayMinutes % 10, digitFour, 0);
  printChar(displayMinutes / 10, digitThree, brightness);
  printChar(displayMinutes % 10, digitFour, brightness);

  //Seconds
  printChar(displaySeconds / 10, digitFive, 0);
  printChar(displaySeconds % 10, digitSix, 0);
  printChar(displaySeconds / 10, digitFive, brightness);
  printChar(displaySeconds % 10, digitSix, brightness);
}

//Read time module values (time and temprature)
void readDS3231(void)
{ 
  second = Clock.getSecond();
  minute = Clock.getMinute();
  hour = Clock.getHour(h12, PM);
  date = Clock.getDate();
  month = Clock.getMonth(Century);
  year = Clock.getYear();
  temperature = Clock.getTemperature();
}

void printChar(int charToPrint, int digit, int brightness)
{
  switch(charToPrint)
  {
    case 0:
      Tlc.set(0 + digit, brightness);
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(4 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      break;
    case 1:
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      break;
    case 2:
      Tlc.set(0 + digit, brightness);
      Tlc.set(1 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(4 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    case 3:
      Tlc.set(0 + digit, brightness);
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    case 4:
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    case 5:
      Tlc.set(0 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    case 6:
      Tlc.set(0 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(4 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    case 7:
      Tlc.set(0 + digit, brightness);
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      break;
    case 8:
      Tlc.set(0 + digit, brightness);
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(4 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    case 9:
      Tlc.set(0 + digit, brightness);
      Tlc.set(1 + digit, brightness);
      Tlc.set(2 + digit, brightness);
      Tlc.set(3 + digit, brightness);
      Tlc.set(5 + digit, brightness);
      Tlc.set(6 + digit, brightness);
      break;
    default:
      break;
  }
  Tlc.update();
  delay(1);
}

void dateDots(int brightness)
{
  Tlc.set(15, brightness);
  Tlc.set(31, brightness);
}

void timeDots(int brightness)
{
  Tlc.set(15, brightness);
  Tlc.set(30, brightness);
  Tlc.set(31, brightness);
  Tlc.set(46, brightness);
}

//Function that read analog input from the potentiometer and return remaped value for the brightness
int readBrightnessControl(void)
{
  int _brightness = 50; //Default brightness 250

  _brightness = map(analogRead(brightnessControler), 0, 1023, 0, 4095);

  if(_brightness < 50)
  {
    _brightness = 50;
  }
  
  return _brightness;
}

//Dots has only one LED and needs lowet voltage
int dotBrightness(void)
{
  int _dotBrightness = 50;

  _dotBrightness = readBrightnessControl() / 5;

  return _dotBrightness;
}


void menu(void)
{
  //Menu loop
  while(menuRunning)
  {    
    //Wait till any button is pressed
    while(buttonState)
    {
      if(digitalRead(menuButton) == HIGH)
      {
        buttonState = 0;
        //Reset menu state
        if(menuState == 3)
        {
          menuState = 0;
          delay(1000); //Debouce
        }
  
        menuState++; //Jumnp to next state
        delay(1000); //Debouce
      }
      
      if(digitalRead(upButton) == HIGH)
      {
        buttonState = 0;
        delay(1000); //Debouce
      }
      
      if(digitalRead(downButton) == HIGH)
      {
        buttonState = 0;
        delay(1000); //Debouce
      }
      //##
      Serial.println("## ## ## DEBUG ## ## ##");
    }
    
    //Check if up button is pushed
    if(digitalRead(upButton) == HIGH)
    {
      if(menuState == 1)  //Increase hours
      {
        setHours++;
        if(setHours == 24)
        {
          setHours = 0;
        }
      }
      if(menuState == 2) //Increase minutes
      {
        setMinutes++;
        if(setMinutes == 60)
        {
          setMinutes = 0;
        }
      }
      if(menuState == 3) //Increase seconds
      {
        setSeconds++;
        if(setSeconds == 60)
        {
          setSeconds = 0;
        }
      }
      delay(1000); //Debouce
    }
    //Check if down button is pushed
    if(digitalRead(downButton) == HIGH)
    {
      if(menuState == 1) //Decrease hours
      {
        setHours--;
        if(setHours <= 0)
        {
          setHours = 23;
        }
      }
      if(menuState == 2) //Decrease minutes
      {
        setMinutes--;
        if(setMinutes <= 0)
        {
          setMinutes = 59;
        }
      }
      if(menuState == 3) //Decrease seconds
      {
        setSeconds--;
        if(setSeconds <= 0)
        {
          setSeconds = 59;
        }
      }
      delay(1000); //Debouce
    }
    //Check if menu button is pushed for exit the menu
    if(digitalRead(menuButton) == HIGH)
    {
      delay(5000); //If menu pressed for 2 seconds, exit menu loop
      if(digitalRead(menuButton) == 0)
      {
        menuRunning = 0;
      }
    }

    //Update display
    if(menuState == 1)
    {
      Tlc.clear();
      printChar(setHours / 10, digitOne, readBrightnessControl());
      printChar(setHours % 10, digitTwo, readBrightnessControl());
    }
    if(menuState == 2)
    {
      Tlc.clear();
      printChar(setMinutes / 10, digitThree, readBrightnessControl());
      printChar(setMinutes % 10, digitFour, readBrightnessControl());
    }
    if(menuState == 3)
    {
      Tlc.clear();
      printChar(setSeconds / 10, digitFive, readBrightnessControl());
      printChar(setSeconds % 10, digitSix, readBrightnessControl());
    }

    buttonState = 1;

    //Set RTC time
    if(menuRunning == 0)
    {
      //##
      //Serial.println("## @@ SEND TIME TO RTC @@ ##");

      Clock.setSecond(setSeconds); //Set seconds
      Clock.setMinute(setMinutes); //Set minutes
      Clock.setHour(setHours); //Set hours

      //Clock.setDate(); //Date
      //Clock.setMonth(); //Month
      //Clock.setYear(); //Year
      
      menuState = 1; //Reset menu state to start from the start in the next menu loop
      buttonState = 0; //Reset button state to update display in the next menu loop
    }
  }
}

void timerMenu(void)
{
  //Timer menu
  while(timerMenuRunning)
  {
    //Wait till any button is pressed
    while(buttonState)
    {
      if(digitalRead(menuButton) == HIGH)
      {
        buttonState = 0;
        //Reset menu state
        if(menuState == 3)
        {
          menuState = 0;
          delay(1000); //Debouce
        }
  
        menuState++; //Jumnp to next state
        delay(1000); //Debouce
      }
      
      if(digitalRead(upButton) == HIGH)
      {
        buttonState = 0;
        delay(1000); //Debouce
      }
      
      if(digitalRead(downButton) == HIGH)
      {
        buttonState = 0;
        delay(1000); //Debouce
      }
      //##
      Serial.println("## ## ## DEBUG ## ## ##");
    }

    //Check if up button is pushed
    if(digitalRead(upButton) == HIGH)  // uint8_t timerHour = 0, timerMinute = 0, timerSecond = 0;
    {
      if(menuState == 1)  //Increase hours
      {
        timerHour++;
        if(timerHour == 24)
        {
          timerHour = 0;
        }
      }
      if(menuState == 2) //Increase minutes
      {
        timerMinute++;
        if(timerMinute == 60)
        {
          timerMinute = 0;
        }
      }
      if(menuState == 3) //Increase seconds
      {
        timerSecond++;
        if(timerSecond == 60)
        {
          timerSecond = 0;
        }
      }
      delay(1000); //Debouce
    }
    //Check if down button is pushed
    if(digitalRead(downButton) == HIGH)
    {
      if(menuState == 1) //Decrease hours
      {
        timerHour--;
        if(timerHour <= 0)
        {
          timerHour = 23;
        }
      }
      if(menuState == 2) //Decrease minutes
      {
        timerMinute--;
        if(timerMinute <= 0)
        {
          timerMinute = 59;
        }
      }
      if(menuState == 3) //Decrease seconds
      {
        timerSecond--;
        if(timerSecond <= 0)
        {
          timerSecond = 59;
        }
      }
      delay(1000); //Debouce
    }
    //Check if menu button is pushed for exit the menu
    if(digitalRead(menuButton) == HIGH)
    {
      delay(5000); //If menu pressed for 2 seconds, exit menu loop
      if(digitalRead(menuButton) == 0)
      {
        timerOn = 1; //Main
        timerMenuRunning = 0;
      }
    }
  
    //Update display
    if(menuState == 1) //Display timer hours
    {
      Tlc.clear();
      printChar(timerHour / 10, digitOne, readBrightnessControl());
      printChar(timerHour % 10, digitTwo, readBrightnessControl());
    }
    if(menuState == 2) //Display timer minutes
    {
      Tlc.clear();
      printChar(timerMinute / 10, digitThree, readBrightnessControl());
      printChar(timerMinute % 10, digitFour, readBrightnessControl());
    }
    if(menuState == 3) //Display timer seconds
    {
      Tlc.clear();
      printChar(timerSecond / 10, digitFive, readBrightnessControl());
      printChar(timerSecond % 10, digitSix, readBrightnessControl());
    }
  
    buttonState = 1;
  }
  
  menuState = 1; //Reset menu state to start from the start in the next menu loop
  buttonState = 0; //Reset button state to update display in the next menu loop
}

void alarmMenu(void)
{
  while(alarmMenuRunning)
  {
    //Wait till any button is pressed
    while(buttonState)
    {
      if(digitalRead(menuButton) == HIGH)
      {
        buttonState = 0;
        //Reset menu state Year, month, date, hour, minute, second
        if(menuState == 6)
        {
          menuState = 0;
          delay(1000); //Debouce
        }
  
        menuState++; //Jumnp to next state
        delay(1000); //Debouce
      }
      
      if(digitalRead(upButton) == HIGH)
      {
        buttonState = 0;
        delay(1000); //Debouce
      }
      
      if(digitalRead(downButton) == HIGH)
      {
        buttonState = 0;
        delay(1000); //Debouce
      }
      //##
      Serial.println("## ## ## DEBUG ## ## ##");
    }
    
    //Check if up button is pushed
    if(digitalRead(upButton) == HIGH)  // uint8_t timerHour = 0, timerMinute = 0, timerSecond = 0;
    {
      if(menuState == 1)  //Increase year
      {
        alarmYear++;
        //if(alarmYear == 24)
        //{
          //alarmYear = 0;
        //}
      }
      if(menuState == 2) //Increase month
      {
        alarmMonth++;
        if(alarmMonth == 13)
        {
          alarmMonth = 1;
        }
      }
      if(menuState == 3) //Increase date
      {
        alarmDate++;
        if(alarmDate == 32)
        {
          alarmDate = 1;
        }
      }
      if(menuState == 4) //Increase hour
      {
        alarmHour++;
        if(alarmHour == 24)
        {
          alarmHour = 0;
        }
      }
      if(menuState == 5) //Increase minute
      {
        alarmMinute++;
        if(alarmMinute == 60)
        {
          alarmMinute = 0;
        }
      }
      if(menuState == 6) //Increase second
      {
        alarmSecond++;
        if(alarmSecond == 60)
        {
          alarmSecond = 0;
        }
      }
      delay(1000); //Debouce
    }
    
    //Check if down button is pushed
    if(digitalRead(downButton) == HIGH)
    {
      if(menuState == 1) //Decrease year
      {
        alarmYear--;
        //if(alarmYear <= 0)
        //{
          //alarmYear = 59;
        //}
      }
      if(menuState == 2) //Decrease month
      {
        alarmMonth--;
        if(alarmMonth <= 0)
        {
          alarmMonth = 12;
        }
      }
      if(menuState == 3) //Decrease date
      {
        alarmDate--;
        if(alarmDate <= 0)
        {
          alarmDate = 59;
        }
      }
      if(menuState == 4) //Decrease hour
      {
        alarmHour--;
        if(alarmHour <= 0)
        {
          alarmHour = 23;
        }
      }
      if(menuState == 5) //Decrease minute
      {
        alarmMinute--;
        if(alarmMinute <= 0)
        {
          alarmMinute = 59;
        }
      }
      if(menuState == 6) //Decrease seconds
      {
        alarmSecond--;
        if(alarmSecond <= 0)
        {
          alarmSecond = 59;
        }
      }
      delay(1000); //Debouce
    }

    //Check if menu button is pushed for exit the menu
    if(digitalRead(menuButton) == HIGH)
    {
      delay(5000); //If menu pressed for 2 seconds, exit menu loop
      if(digitalRead(menuButton) == 0)
      {
        alarmMenuRunning = 0;
      }
    }
  
    //Update display
    if(menuState == 1) //Display timer hours
    {
      Tlc.clear();
      printChar(alarmYear / 10, digitOne, readBrightnessControl());
      printChar(alarmYear % 10, digitTwo, readBrightnessControl());
    }
    if(menuState == 2) //Display timer minutes
    {
      Tlc.clear();
      printChar(alarmMonth / 10, digitThree, readBrightnessControl());
      printChar(alarmMonth % 10, digitFour, readBrightnessControl());
    }
    if(menuState == 3) //Display timer seconds
    {
      Tlc.clear();
      printChar(alarmDate / 10, digitFive, readBrightnessControl());
      printChar(alarmDate % 10, digitSix, readBrightnessControl());
    }
    if(menuState == 4) //Display timer seconds
    {
      Tlc.clear();
      printChar(alarmHour / 10, digitOne, readBrightnessControl());
      printChar(alarmHour % 10, digitTwo, readBrightnessControl());
    }
    if(menuState == 5) //Display timer seconds
    {
      Tlc.clear();
      printChar(alarmMinute / 10, digitThree, readBrightnessControl());
      printChar(alarmMinute % 10, digitFour, readBrightnessControl());
    }
    if(menuState == 6) //Display timer seconds
    {
      Tlc.clear();
      printChar(alarmSecond / 10, digitFive, readBrightnessControl());
      printChar(alarmSecond % 10, digitSix, readBrightnessControl());
    }
  
    buttonState = 1;
  }

  menuState = 1; //Reset menu state to start from the start in the next menu loop
  buttonState = 0; //Reset button state to update display in the next menu loop

  //Set alarm 1
  Clock.setA1Time(alarmDate, alarmHour, alarmMinute, alarmSecond, 0, false, false, false);

  //Enable alarm 1
  Clock.turnOnAlarm(1);
  alarmOn = 1;
  delay(1000); //Debouce
}

